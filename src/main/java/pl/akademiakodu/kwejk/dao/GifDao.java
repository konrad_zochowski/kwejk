package pl.akademiakodu.kwejk.dao;

import pl.akademiakodu.kwejk.model.Gif;

import java.util.List;

/**
 * Created by slickender on 04.09.2017.
 */
public interface GifDao {
   Gif findOne(String name);
   List<Gif> findFavorites();
}
